let {element, createApp} = require('deku');
let {createStore, applyMiddleware} = require('redux');
let R = require('ramda');

let reducer = require('./reducer');
let Story = require('./story');

function updateStory() {
  let oReq = new XMLHttpRequest();
  oReq.addEventListener('load', () => {
    if (oReq.statusText === 'OK') {
      store.dispatch({type: 'UPDATE', data: JSON.parse(oReq.responseText)});
    } else {
      setTimeout(
        updateStory,
        1000
      );
    }
  });
  oReq.open('GET', '/story/' + storyName + '/data');
  oReq.send();
}

const scheduleUpdate = store => next => action => {
  if (action.type === 'UPDATE') {
    let delay = action.data.currentRound.timeLeft;
    let timeoutId = setTimeout(
      updateStory,
      delay
    );
    return next(action);
  } else {
    return next(action);
  }
};

const updateRemaining = store => next => action => {
  if (action.type === 'REMAINING') {
    let delay = 100;
    let round = store.getState().external.currentRound;
    let remaining = round ? (round.endTime - Date.now()) / (round.endTime - round.startTime) : 0;
    let timeoutId = setTimeout(
      () => store.dispatch({type: 'REMAINING', value: remaining}),
      delay
    );
    return next(action);
  } else {
    return next(action);
  }
};

const hitServer = store => next => action => {
  if (!action.meta || !action.meta.server) {
    return next(action);
  }

  let oReq = new XMLHttpRequest();
  oReq.addEventListener('load', () => store.dispatch(JSON.parse(oReq.responseText).success === true ? R.set(R.lensProp('meta'), R.omit(['server'], action.meta), action) : {type: 'FAILED', action: action}));
  let payload = {};
  switch (action.type){
    case 'VOTE':
      payload = {password:'PASS', storyName: storyName, uID: action.uID};
      oReq.open('POST', '/vote');
      break;
    case 'SUBMIT':
      payload = {password:'PASS', storyName: storyName, snippet: action.snippet || '', author: {name: store.getState().author}};
      oReq.open('POST', '/submit');
      break;
    default:
      payload = {};
  }

  oReq.setRequestHeader("Content-type", "application/json");
  oReq.send(JSON.stringify(payload));
  return next({type: 'NOTHING'});
};

let storyName = unescape(/story=(\w*)/.exec(document.location.search)[1]);
console.log(storyName);

let store = createStore(reducer, applyMiddleware(scheduleUpdate, updateRemaining, hitServer));
store.subscribe(() => console.log(store.getState()));

let render = createApp(document.getElementById('app'), store.dispatch);

store.subscribe(() => {
  render(
    element(Story),
    store.getState()
  );
});

updateStory();
store.dispatch({type: 'REMAINING', value: 0});
