let {element} = require('deku');

let Countdown = {
  render: ({props}) => {
    return element('meter', {class: 'block meter', low: '0.15', high: '0.5', optimum: '0.99', value: props.remaining});
  }
};

module.exports = Countdown;
