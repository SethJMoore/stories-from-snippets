let R = require('ramda');

function reducer(state = {remaining: 0.0, external: {}, author: 'anonymous'}, action){
  switch (action.type){
    case 'UPDATE':
      let endTime = Date.now() + action.data.currentRound.timeLeft;
      let newCurrentRound = {
        endTime: endTime,
        startTime: endTime - action.data.currentRound.totalTime
      }
      return R.merge(R.omit(['vote', 'submitted'], state), {external: R.set(R.lensProp('currentRound'), newCurrentRound, action.data)});
    case 'REMAINING':
      let remainingLens = R.lensProp('remaining');
      return R.set(remainingLens, action.value, state);
    case 'VOTE':
      return R.merge(R.set(R.lensProp('external'), R.omit(['vote'], R.view(R.lensProp('external'), state)), state), {vote: state.snippet, snippet: ''});
    case 'INPUT':
      return R.set(R.lensProp('snippet'), action.value, state);
    case 'SUBMIT':
      return R.omit(['snippet'], R.set(R.lensProp('submitted'), action.snippet, state));
    case 'NOTHING':
      return state;
    case 'FAILED':
      return R.merge(state, {failed: action.action});
    case 'SET_AUTHOR':
      return R.set(R.lensProp('author'), action.author, state);
    default:
      return state;
  }
};

module.exports = reducer;
