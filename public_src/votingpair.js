let {element} = require('deku');

let Snippet = require('./snippet');

let VotingPair = {
  render: ({context, dispatch}) => {
    let pair = context.external.vote;
    if (pair) {
      let element2 = undefined;
      if (pair.length > 1) {
        element2 = element('div', {class: 'vote block', onMouseEnter: () => dispatch({type: 'INPUT', value: pair[1].snippet}), onMouseLeave: () => dispatch({type: 'INPUT', value: ''}), onClick: () => dispatch({type: 'VOTE', uID: pair[1].uID, meta: {server: true}})}, element(Snippet, {}, pair[1].snippet));
      } else {
        element2 = element('div', {class: 'vote block red', onClick: () => dispatch({type: 'VOTE', uID: '', meta: {server: true}})}, 'NOTHING');
      }
      return element('div', {class: 'voting'},
        [
          element('div', {class: 'vote block', onMouseEnter: () => dispatch({type: 'INPUT', value: pair[0].snippet}), onMouseLeave: () => dispatch({type: 'INPUT', value: ''}), onClick: () => dispatch({type: 'VOTE', uID: pair[0].uID, meta: {server: true}})}, element(Snippet, {}, pair[0].snippet)),
          element2
        ]
      );
    } else if (context.vote != undefined) {
      return element('div', {class: 'voting'},
        [
          element('div', {class: 'vote block disabled'}, [element('h3', {}, 'Your vote:'), element(Snippet, {}, context.vote)])
        ]
      );
    }else {
      return element('span', {class: 'ignore'});
    }
  }
};

module.exports = VotingPair;
