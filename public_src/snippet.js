let {element} = require('deku');

let Snippet = {
  render: ({props, children}) => {
    return element('span', props, children);
  }
};

module.exports = Snippet;
