let {element} = require('deku');
let R = require('ramda');

let Snippet = require('./snippet');
let Countdown = require('./countdown');
let VotingPair = require('./votingpair');
let Submission = require('./submission');

let Story = {
  render: ({props, children, context}) => {
    if (context.external.story) {
      return element('div', {class: 'story block'},
        [
          element('div', {class: 'block-group'}, element(Countdown, {remaining: context.remaining})),
          context.external.vote || context.vote != undefined ? element(VotingPair) :
                                  element(Submission),
          element('div', {class: 'block'},
            R.append(element(Snippet, {class: 'snippet grey'}, [context.snippet || '', element('div', {class: 'author'}, 'Author: ' + context.author)]), R.map((x) => element(
                Snippet,
                {class: 'snippet', 'data-uID': x.uID},
                [x.snippet, element('div', {class: 'author'}, 'Author: ' + x.author.name)]),
              context.external.story
            ))
          )
        ]
      );
    } else {
      return element('div', {class: 'story'}, ['']);
    }
  }
};

module.exports = Story;
