let {element} = require('deku');

let Submission = {
  render: ({props, dispatch, context}) => {
    return element('div', {class: 'block-group submission'},
      [
        context.submitted != undefined ? element('div', {class: 'vote block disabled'}, [element('h3', {}, 'You submitted:'), element('span', {}, context.submitted)]) : element('span', {class: 'ignore'}),
        element('textarea', {class: 'block snippet-input', value: context.snippet, onInput: (e) => dispatch({type: 'INPUT', value: e.target.value})}),
        element('button', {class: 'block submit-button', onClick: () => dispatch({type: 'SUBMIT', snippet: context.snippet, meta: {server: true}})}, 'Submit'),
        element('div', {class: 'block author-input' + (context.author === 'anonymous' ? ' red' : '')}, [
          element('label', {class: 'block-group'}, [
              element('span', {class: 'block'}, 'Author:'),
              element('input', {class: 'block', type: 'text', placeholder: context.author, onInput: (e) => dispatch({type: 'SET_AUTHOR', author: e.target.value})}),
          ])
        ])
      ]
    );
  }
};

module.exports = Submission;
