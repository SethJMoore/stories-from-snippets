var path = require('path');
console.log(path.join(__dirname, 'node_modules'));
module.exports = {
  entry: [
    './public_src/index.js'
  ],
//  module: {
//    loaders: [{
//      test: /\.jsx?$/,
//      include: [
//        path.resolve(__dirname, 'public_src')
//      ],
//      loader: 'babel'
//    }]
//  },
//  resolve: {
//    root: path.join(__dirname, 'node_modules'),
//    extensions: ['', '.js', '.jsx']
//  },
//  resolveLoader: {
//    root: path.join(__dirname, 'node_modules')
//  },
  output: {
    path: __dirname + '/public',
    publicPath: '/',
    filename: 'bundle.js'
  }
};
