import express from 'express';
import bodyParser from 'body-parser';
import shortid from 'shortid'

export default function startServer(store, buildStorySettings) {
  const server = express();
  const prepareStoryData = (data) => {
    let source = data.toJS();
    let prepared = {
      currentRound: {
        timeLeft: source.currentRound.endTime - Date.now(),
        totalTime: source.currentRound.endTime - source.currentRound.startTime
      },
      //currentRound: source.currentRound,
      story: source.story,
      vote: source.vote ? source.vote.pair : undefined
    };
    for (let i in prepared.vote) {
      delete prepared.vote[i].author;
    }
    return prepared;
  };
  server.use(express.static('public'));
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({extended: true}));

  server.get('/story/:storyName', (req, res, next) => {
    res.redirect(303, '/story.html?story=' + req.params.storyName);
  });

  server.get('/story/:storyName/data', (req, res, next) => {
    // TODO: Require a password.
    let story = store.getState().getIn(['stories', req.params.storyName]);
    if (story) {
      res.json(prepareStoryData(story));
    } else {
      res.redirect(404, '/index.html');
//      let now = Date.now();
//      let action = {
//        type: 'CREATE_STORY',
//        settings: {
//          [req.params.storyName]: buildStorySettings({startTime: now})
//        }
//      };
//      store.dispatch(action);
//      res.json(prepareStoryData(store.getState().getIn(['stories', req.params.storyName])));
    }
  });

  server.post('/create', (req, res, next) => {
    if (store.getState().getIn(['stories', req.body.storyName])) {
      res.redirect(409, '/index.html');
    } else {
      let now = Date.now();
      let action = {
        type: 'CREATE_STORY',
        settings: {
          [req.body.storyName]: buildStorySettings({startTime: now, password: req.body.password, submittingInterval: parseInt(req.body.submitting), votingInterval: parseInt(req.body.voting)})
        }
      };
      store.dispatch(action);
      res.redirect(303, '/story.html?story=' + req.body.storyName);
    }
  });

  server.post('/submit', (req, res, next) => {
    let previousState = store.getState();
    // TODO: Sanitize the values going into submission to make sure they're strings.
    let submission = {
      author: {
        name: req.body.author ? req.body.author.name : 'Anonymous'
      },
      password: req.body.password,
      snippet: req.body.snippet,
      storyName: req.body.storyName,
      uID: shortid.generate()
    };
    store.dispatch({type: 'SUBMIT', submission: submission});
    if (previousState != store.getState()) {
      res.json({success: true});
    } else {
      res.json({success: false});
    }
  });

  server.post('/vote', (req, res, next) => {
    let previousState = store.getState();
    // TODO: Sanitize the values going into voteData to make sure they're strings.
    let voteData = {
      password: req.body.password,
      storyName: req.body.storyName,
      uIDOfSubmission: req.body.uID
    };
    store.dispatch({type: 'VOTE', voteData: voteData});
    if (previousState != store.getState()) {
      res.json({success: true});
    } else {
      res.json({success: false});
    }
  });

  /*
   * DANGEROUS!!
   * TODO: Make it harder to delete stories.
   */
  server.get('/delete/:storyName', (req, res, next) => {
    let action = {type: 'DELETE_STORY', storyName: req.params.storyName};
    store.dispatch(action);
    res.json(store.getState().getIn(['stories', req.params.storyName], {toJS: () => 'NOTHING'}).toJS());
  });
  server.listen(3000);
}
