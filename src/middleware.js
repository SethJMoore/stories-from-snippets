
/**
 * Sets up a timer to call NEXT at the end of the next round of the story being created or NEXTed.
 */
export const addNextTimeout = store => next => action => {
  let result = next(action);
  if (action.type === 'CREATE_STORY') {
    let storyName = Object.keys(action.settings)[0];
    let story = store.getState().getIn(['stories', storyName], {toJS: () => ''}).toJS();
    if (story) {
      setTimeout(() => store.dispatch({type: 'NEXT', storyName: storyName}), story.currentRound.endTime - Date.now());
    }
  } else if (action.type === 'NEXT') {
    let story = store.getState().getIn(['stories', action.storyName], {toJS: () => ''}).toJS();
    if (story) {
      if (Date.now() < story.expiration) {
        setTimeout(() => store.dispatch({type: 'NEXT', storyName: action.storyName}), story.currentRound.endTime - Date.now());
      } else {
        setTimeout(() => store.dispatch({type: 'DELETE_STORY', storyName: action.storyName}), 5000);
      }
    }
  }
  return result;
};

/**
 * Logs all actions and states after they are dispatched.
 */
export const logger = store => next => action => {
  console.log('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  return result;
}
