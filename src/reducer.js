import {createStory, submit, next, vote, deleteStory, INITIAL_STATE} from './core';

export default function reducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'CREATE_STORY':
      return createStory(state, action.settings);
    case 'SUBMIT':
      return submit(state, action.submission);
    case 'NEXT':
      return next(state, action.storyName);
    case 'VOTE':
      return vote(state, action.voteData);
    case 'DELETE_STORY':
      return deleteStory(state, action.storyName);
  }
  return state;
}
