import {createStore, applyMiddleware} from 'redux';
import reducer from './reducer';
import {addNextTimeout, logger} from './middleware';

export default function makeStore() {

  return createStore(reducer, applyMiddleware(addNextTimeout, logger));
}
