import {Map, List, fromJS} from 'immutable';

export const INITIAL_STATE = Map();

export function createStory(state = Map(), story) {
  const immutableStory = fromJS(story);
  return state.hasIn(['stories', immutableStory.keySeq().first()]) ?
    state :
    state.setIn(['stories', immutableStory.keySeq().first()], immutableStory.valueSeq().first());
}

export function submit(state, submission) {
  const immutableSubmission = fromJS(submission).remove('storyName').remove('password');
  return state.hasIn(['stories', submission.storyName])
    && state.getIn(['stories', submission.storyName, 'password']) === submission.password 
    && !state.hasIn(['stories', submission.storyName, 'vote']) ?
    state.setIn(['stories', submission.storyName, 'submissions'], state.getIn(['stories', submission.storyName, 'submissions'], List()).push(immutableSubmission)) :
    state;
}

export function next(state, storyName) {
  if (!state.hasIn(['stories', storyName])) return state;
  const round = state.getIn(['stories', storyName, 'currentRound']);
  if (!state.hasIn(['stories', storyName, 'vote'])) {

    return state.getIn(['stories', storyName, 'submissions']) === List() ?

      state.setIn(['stories', storyName, 'currentRound', 'startTime'], round.get('endTime'))
           .setIn(['stories', storyName, 'currentRound', 'endTime'], round.get('endTime') + state.getIn(['stories', storyName, 'submittingInterval'])) :

      // TODO: Possibly use withMutations, mergeDeep, or plain JS object instead of creating all these intermediate objects with setIn.
      state.setIn(['stories', storyName, 'currentRound', 'startTime'], round.get('endTime'))
           .setIn(['stories', storyName, 'currentRound', 'endTime'], round.get('endTime') + state.getIn(['stories', storyName, 'votingInterval']))
           .setIn(['stories', storyName, 'vote', 'pair'], state.getIn(['stories', storyName, 'submissions']).slice(0, 2))
           .setIn(['stories', storyName, 'vote', 'tally'], fromJS({[state.getIn(['stories', storyName, 'submissions', 0, 'uID'], '')]: 0,
                                                                   [state.getIn(['stories', storyName, 'submissions', 1, 'uID'], '')]: 0}))
           .setIn(['stories', storyName, 'submissions'], state.getIn(['stories', storyName, 'submissions']).skip(2));
  }

  if (state.hasIn(['stories', storyName, 'vote'])) {

    let mState = state.getIn(['stories', storyName]).toJS();
    const uIDsUnderVote = Object.keys(mState.vote.tally);
    mState.vote.pair = mState.vote.tally[uIDsUnderVote[0]] > mState.vote.tally[uIDsUnderVote[1]] ?
      [mState.vote.pair.find((element) => element.uID === uIDsUnderVote[0])] :
      mState.vote.tally[uIDsUnderVote[1]] > mState.vote.tally[uIDsUnderVote[0]] ?
        [mState.vote.pair.find((element) => element.uID === uIDsUnderVote[1])] :
        mState.vote.pair;
    if (mState.vote.pair.length === 1 && uIDsUnderVote[1] === '' && (mState.vote.tally[uIDsUnderVote[0]] !== mState.vote.tally[uIDsUnderVote[1]])) {
      if (mState.vote.pair[0]) {
        mState.story.push(...mState.vote.pair);
      }
      delete mState.vote;
      mState.currentRound.startTime = mState.currentRound.endTime;
      mState.currentRound.endTime = mState.currentRound.startTime + mState.submittingInterval;
    } else {
      mState.submissions.push(...mState.vote.pair);
      mState.vote.pair = mState.submissions.slice(0,2);
      mState.vote.tally = {[mState.vote.pair[0].uID]: 0,
        [mState.vote.pair[1] ? mState.vote.pair[1].uID : '']: 0};
      mState.submissions = mState.submissions.slice(2);
      mState.currentRound.startTime = mState.currentRound.endTime;
      mState.currentRound.endTime = mState.currentRound.startTime + mState.votingInterval;
    }
    return state.setIn(['stories', storyName], fromJS(mState));
  }
}

export function vote(state, voteData) {
  let voteState = state.getIn(['stories', voteData.storyName, 'vote']);
  if (voteState === undefined) return state;
  if (state.getIn(['stories', voteData.storyName, 'password']) !== voteData.password) return state;
  if (voteState.hasIn(['tally', voteData.uIDOfSubmission])) {
    voteState = voteState.setIn(['tally', voteData.uIDOfSubmission], voteState.getIn(['tally', voteData.uIDOfSubmission]) + 1);
    return state.setIn(['stories', voteData.storyName, 'vote'], voteState);
  } else {
    return state;
  }
}

export function deleteStory(state, storyName) {
  return state.deleteIn(['stories', storyName]);
}
