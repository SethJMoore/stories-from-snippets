import {Map} from 'immutable';
import {expect} from 'chai';

import makeStore from '../src/store';

describe('store', function() {
  it('is a Redux store configured with the correct reducer', function() {
    const store = makeStore();
    expect(store.getState()).to.equal(Map());

    store.dispatch({type: 'CREATE_STORY', settings: {story1: {currentRound: {startTime: 0, endTime: 60000, }, password: 'PASS', submittingInterval: 60000, votingInterval: 10000, story: [], submissions: [] }}});
    expect(store.getState().hasIn(['stories', 'story1'])).to.equal(true);
  });
});
