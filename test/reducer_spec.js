import {Map} from 'immutable';
import {expect} from 'chai';

import reducer from '../src/reducer';

describe('reducer', function() {

  const story = {
    story1: {
      currentRound: {
        startTime: 0,
        endTime: 60000,
      },
      password: 'PASS',
      submissionInterval: 60000,
      votingInterval: 10000,
      story: [],
      submissions: []
    }
  };

  const submission = {
    storyName: 'story1',
    password: 'PASS',
    uID: 'unique',
    snippet: 'Thus the story begins',
    author: {
      name: 'Mr. Writer'
    }
  };

  it('handles CREATE_STORY', function() {
    const initialState = Map();
    const action = {
      type: 'CREATE_STORY',
      settings: story
    };
    const nextState = reducer(initialState, action);
    expect(nextState.hasIn(['stories', 'story1'])).to.equal(true);
  });

  it('handles SUBMIT', function() {
    const initialState = reducer(Map(), {type: 'CREATE_STORY', settings: story});
    const action = {
      type: 'SUBMIT',
      submission: submission
    };
    const nextState = reducer(initialState, action);
    expect(nextState.getIn(['stories', 'story1', 'submissions']).size).to.equal(1);
  });

  it('handles NEXT', function() {
    const initialState = reducer(reducer(Map(), {type: 'CREATE_STORY', settings: story}), {type: 'SUBMIT', submission: submission});
    const action = {
      type: 'NEXT',
      storyName: 'story1'
    };
    const nextState = reducer(initialState, action);
    expect(nextState.hasIn(['stories', 'story1', 'vote'])).to.equal(true);
  });

  it('handles VOTE', function() {
    const actions = [{type: 'CREATE_STORY', settings: story},
                     {type: 'SUBMIT', submission: submission},
                     { type: 'NEXT', storyName: 'story1' }]
    const initialState = actions.reduce(reducer, Map());
    const action = {
      type: 'VOTE',
      voteData: {
        storyName: 'story1',
        password: 'PASS',
        uIDOfSubmission: 'unique'
      }
    }
    const nextState = reducer(initialState, action);
    expect(nextState.getIn(['stories', 'story1', 'vote', 'tally', 'unique'])).to.equal(1);
  });

  it('handles DELETE_STORY', function() {
    const initialState = reducer(Map(), {type: 'CREATE_STORY', settings: story});
    const action = {
      type: 'DELETE_STORY',
      storyName: 'story1'
    };
    const nextState = reducer(initialState, action);
    expect(nextState.hasIn(['stories', 'story1'])).to.equal(false);
  });

  it('has an initial state', function() {
    const action = {type: 'CREATE_STORY', settings: story};
    const nextState = reducer(undefined, action);
    expect(nextState.hasIn(['stories', 'story1'])).to.equal(true);
  });

});
