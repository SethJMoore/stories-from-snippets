import {List, Map, fromJS} from 'immutable';
import {expect} from 'chai';

import {createStory, submit, next, vote, deleteStory} from '../src/core';

describe('application logic', function() {

  describe('createStory', function() {

    const newStory1 = {
      story1: {
        currentRound: {
          startTime: 0,
          endTime: 60000,
        },
        password: 'PASS',
        submissionInterval: 60000,
        votingInterval: 10000,
        story: [],
        submissions: []
      }
    };

    const newStory2 = {
      story2: {
        currentRound: {
          startTime: 70000,
          endTime: 130000,
        },
        password: 'PASS',
        submissionInterval: 60000,
        votingInterval: 10000,
        story: [],
        submissions: []
      }
    };

    it('should return a state with a story under stories when state is undefined', function() {
      const state = undefined;
      const nextState = createStory(state, newStory1);
      expect(nextState).to.equal(Map({
        stories: fromJS(newStory1)
      }));
    });

    it('should add a story under stories to an empty state', function() {
      const state = Map();
      const nextState = createStory(state, newStory1);
      expect(nextState).to.equal(Map({
        stories: fromJS(newStory1)
      }));
    });

    it('should add a story to the state when stories is defined', function() {
      const state = fromJS({stories: {}});
      const nextState = createStory(state, newStory1);
      expect(nextState).to.equal(Map({
        stories: fromJS(newStory1)
      }));
    });

    it('should add a story to the state when other stories exist', function() {
      const state = createStory(undefined, newStory1);
      const nextState = createStory(state, newStory2);
      expect(nextState).to.equal(Map({
        stories: fromJS({story1: newStory1.story1, story2: newStory2.story2})
      }));
    });

    it('should return unaltered state if story of the same name already exists', function() {
      const state = createStory(state, newStory1);
      const nextState = createStory(state, {story1: newStory2.story2});
      expect(nextState).to.equal(state);
    });

  });

  describe('submit', function() {
    const state = fromJS({
      stories: {
        storyname: {
          password: 'PASS',
          submissions: [{snippet: 'This is the real beginning.', author: {name: 'Joe'}, uID: 'JKL'}]
        }
      }
    });

    it('should add a submission to the story with matching name and password', function() {
      const submission = {
        storyName: 'storyname',
        password: 'PASS',
        uID: 'unique',
        snippet: 'Thus the story begins',
        author: {
          name: 'Mr. Writer'
        }
      };
      const nextState = submit(state, submission);
      expect(nextState).to.equal(fromJS({
        stories: {
          storyname: {
            password: 'PASS',
            submissions: [{snippet: 'This is the real beginning.',
                           author: {name: 'Joe'},
                           uID: 'JKL'},

                          {snippet: submission.snippet,
                           author: submission.author,
                           uID: submission.uID}]
          }
        }
      }));
    });

    it('should return unaltered state if password doesn\'t match', function() {
      const submission = {
        storyName: 'storyname',
        password: 'SSAP',
        uID: 'unique',
        snippet: 'Thus the story begins',
        author: {
          name: 'Mr. Writer'
        }
      };
      const nextState = submit(state, submission);
      expect(nextState).to.equal(state);
    });

    it('should return unaltered state if no matching storyName found', function() {
      const submission = {
        storyName: 'wrongname',
        password: 'PASS',
        uID: 'unique',
        snippet: 'Thus the story begins',
        author: {
          name: 'Mr. Writer'
        }
      };
      const nextState = submit(state, submission);
      expect(nextState).to.equal(state);
    });

    it('should return unaltered state if vote exists', function() {
      const localState = state.setIn(['stories', 'storyname', 'vote'], Map());
      const submission = {
        storyName: 'storyname',
        password: 'PASS',
        uID: 'unique',
        snippet: 'Thus the story begins',
        author: {
          name: 'Mr. Writer'
        }
      };
      const nextState = submit(localState, submission);
      expect(nextState).to.equal(localState);
    });

  });

  describe('next', function() {

    const state = createStory(undefined, {
      storyname: {
        password: 'PASS',
        currentRound: {
          startTime: 0,
          endTime: 60000
        },
        votingInterval: 10000,
        submittingInterval: 60000,
        story: [],
        submissions: []
      }
    });

    const submission = {
      storyName: 'storyname',
      password: 'PASS',
      uID: 'unique1',
      snippet: 'Thus the story begins',
      author: {
        name: 'Mr. Writer'
      }
    };

    it('should return unaltered state if specified story does not exist', function() {
      const storyName = 'wrongStoryName';
      const nextState = next(state, storyName);
      expect(nextState).to.equal(state);
    });

    it('should update to new start and end times if currently submitting and there are no submissions', function() {
      const storyName = 'storyname';
      const nextState = next(state, storyName);
      expect(nextState).to.equal(state.setIn(['stories', storyName, 'currentRound'], fromJS({startTime: 60000, endTime: 120000})));
    });

    it('should change round to "voting" and update startTime and endTime, based on votingInterval, if currently submitting and there are submissions', function() {
      const localState = submit(state, {snippet: 'words', author: {name: 'Joe'}, uID: 'bla', storyName: 'storyname', password: 'PASS'});
      const storyName = 'storyname';
      const nextState = next(localState, storyName);
      expect(nextState.getIn(['stories', 'storyname', 'currentRound'])).to.equal(fromJS({startTime: 60000, endTime: 70000}));
    });

    it('should create vote and move 2 submissions from submissions into vote on switch to voting', function() {

      const localState = submit(submit(submit(state, submission), submission), submission);
      const nextState = next(localState, 'storyname');
      expect(nextState.getIn(['stories', 'storyname', 'submissions'])).to.equal(localState.getIn(['stories', 'storyname', 'submissions']).skip(2));
      expect(nextState.getIn(['stories', 'storyname', 'vote', 'pair'])).to.equal(localState.getIn(['stories', 'storyname', 'submissions']).slice(0, 2));
    });

    it('should move voting pair back to submissions, and get another two from submissions if no winner', function() {
      const localState = submit(submit(submit(state, submission), Object.defineProperty(JSON.parse(JSON.stringify(submission)), 'uID', {value: 'u2'})), Object.defineProperty(JSON.parse(JSON.stringify(submission)), 'uID', {value: 'u3'}));
      const nextState = next(next(localState, 'storyname'), 'storyname');
      expect(nextState.getIn(['stories', 'storyname', 'submissions'])).to.equal(localState.getIn(['stories', 'storyname', 'submissions']).slice(1,2));
      expect(nextState.getIn(['stories', 'storyname', 'vote', 'pair'])).to.equal(localState.getIn(['stories', 'storyname', 'submissions']).filter((v,k) => k === 0 || k === 2).reverse());
    });

    it('should update startTime and endTime based on votingInterval if in the middle of voting', function() {
      const localState = next(submit(submit(submit(state, submission), submission), submission), 'storyname');
      const nextState = next(localState, 'storyname');
      expect(nextState.getIn(['stories', 'storyname', 'currentRound', 'startTime'])).to.equal(localState.getIn(['stories', 'storyname', 'currentRound', 'endTime']));
      expect(nextState.getIn(['stories', 'storyname', 'currentRound', 'endTime'])).to.equal(localState.getIn(['stories', 'storyname', 'currentRound', 'endTime']) + localState.getIn(['stories', 'storyname', 'votingInterval']));
    });

    it('should move winning submission back to submissions, and get another two from submissions', function() {
      const localState = submit(submit(submit(state, submission), Object.defineProperty(JSON.parse(JSON.stringify(submission)), 'uID', {value: 'u2'})), Object.defineProperty(JSON.parse(JSON.stringify(submission)), 'uID', {value: 'u3'}));
      const nextState = next(next(localState, 'storyname').setIn(['stories', 'storyname', 'vote', 'tally', 'u2'], 1), 'storyname');
      expect(nextState.getIn(['stories', 'storyname', 'submissions'])).to.equal(List());
      expect(nextState.getIn(['stories', 'storyname', 'vote', 'pair'])).to.equal(List([localState.getIn(['stories', 'storyname', 'submissions', 2]), localState.getIn(['stories', 'storyname', 'submissions', 1])]));
    });

    it('should set up voting on last submission vs no submission when 1 submission is left', function() {
      const localState = submit(state, submission);
      const nextState = next(localState, 'storyname');
      expect(nextState.getIn(['stories', 'storyname', 'submissions'])).to.equal(List());
      expect(nextState.getIn(['stories', 'storyname', 'vote', 'pair'])).to.equal(List([localState.getIn(['stories', 'storyname', 'submissions', 0])]));
    });

    it('should run the vote again if submission vs no submission is tied', function() {
      const localState = next(submit(state, submission), 'storyname');
      console.log(localState);
      const nextState = next(localState, 'storyname');
      console.log(nextState);
      expect(nextState.getIn(['stories', 'storyname', 'submissions'])).to.equal(List());
      expect(nextState.getIn(['stories', 'storyname', 'vote', 'pair'])).to.equal(localState.getIn(['stories', 'storyname', 'vote', 'pair']));
    });

    it('should append winning submission to the story, remove \'vote\', and update times based on submission interval when there is a final winner', function() {
      const localState = next(submit(state, submission), 'storyname').setIn(['stories', 'storyname', 'vote', 'tally', 'unique1'], 1);
      const nextState = next(localState, 'storyname');
      let localSubmission = JSON.parse(JSON.stringify(submission));
      delete localSubmission.storyName;
      delete localSubmission.password;
      expect(nextState.getIn(['stories', 'storyname', 'story'])).to.equal(fromJS([localSubmission]));
      expect(nextState.getIn(['stories', 'storyname', 'vote'], undefined)).to.equal(undefined);
      expect(nextState.getIn(['stories', 'storyname', 'currentRound'])).to.equal(fromJS({startTime: localState.getIn(['stories', 'storyname', 'currentRound', 'endTime']),
                                                                                         endTime: localState.getIn(['stories', 'storyname', 'currentRound', 'endTime'])
                                                                                                  + localState.getIn(['stories', 'storyname', 'submittingInterval'])}));
    });

    it('should not append anything to the story if no submission wins', function() {
      const localState = next(submit(state, submission), 'storyname').setIn(['stories', 'storyname', 'vote', 'tally', ''], 1);
      const nextState = next(localState, 'storyname');
      expect(nextState.getIn(['stories', 'storyname', 'story'])).to.equal(state.getIn(['stories', 'storyname', 'story']));
    });

  });

  describe('vote', function() {

    const state = createStory(undefined, {
      storyname: {
        password: 'PASS',
        currentRound: {
          startTime: 0,
          endTime: 60000
        },
        votingInterval: 10000,
        submittingInterval: 60000,
        story: [],
        submissions: []
      }
    });

    const submission1 = { storyName: 'storyname', password: 'PASS', uID: 'unique1', snippet: 'Thus the story begins', author: { name: 'Mr. Writer' } };
    const submission2 = { storyName: 'storyname', password: 'PASS', uID: 'unique2', snippet: 'Thus the story begins', author: { name: 'Mr. Writer' } };

    it('should return unaltered state if currently in submitting round', function() {
      const localState = submit(submit(state, submission1), submission2);
      let voteData = {storyName: 'storyname', password: 'PASS', uIDOfSubmission: 'unique1'};
      const nextState = vote(localState, voteData);
      expect(nextState).to.equal(localState);
    });

    it('should add 1 to tally of the submission being voted for', function() {
      const localState = next(submit(submit(state, submission1), submission2), 'storyname');
      let voteData = {storyName: 'storyname', password: 'PASS', uIDOfSubmission: 'unique1'};
      const nextState = vote(localState, voteData);
      expect(nextState.getIn(['stories', 'storyname', 'vote', 'tally', 'unique1'])).to.equal(1);
      expect(nextState.getIn(['stories', 'storyname', 'vote', 'tally', 'unique2'])).to.equal(0);
      voteData.uIDOfSubmission = 'unique2';
      const nextState2 = vote(nextState, voteData);
      expect(nextState2.getIn(['stories', 'storyname', 'vote', 'tally', 'unique1'])).to.equal(1);
      expect(nextState2.getIn(['stories', 'storyname', 'vote', 'tally', 'unique2'])).to.equal(1);
    });

    it('should return unaltered state if story doesn\'t exist', function() {
      const localState = next(submit(submit(state, submission1), submission2), 'storyname');
      let voteData = {storyName: 'wrongname', password: 'PASS', uIDOfSubmission: 'unique1'};
      const nextState = vote(localState, voteData);
      expect(nextState).to.equal(localState);
    });

    it('should return unaltered state if wrong password', function() {
      const localState = next(submit(submit(state, submission1), submission2), 'storyname');
      let voteData = {storyName: 'storyname', password: 'You shall not pass.', uIDOfSubmission: 'unique1'};
      const nextState = vote(localState, voteData);
      expect(nextState).to.equal(localState);
    });

    it('should return unaltered state if submission doesn\'t exist', function() {
      const localState = next(submit(submit(state, submission1), submission2), 'storyname');
      let voteData = {storyName: 'storyname', password: 'PASS', uIDOfSubmission: 'wrongUID'};
      const nextState = vote(localState, voteData);
      expect(nextState).to.equal(localState);
    });
  });

  describe('deleteStory', function() {

    const story1 = {
      storyname: {
        password: 'PASS',
        currentRound: {
          startTime: 0,
          endTime: 60000
        },
        votingInterval: 10000,
        submittingInterval: 60000,
        story: [],
        submissions: []
      }
    }
    const story2 = {
      storyname2: {
        password: 'PASS',
        currentRound: {
          startTime: 0,
          endTime: 60000
        },
        votingInterval: 10000,
        submittingInterval: 60000,
        story: [],
        submissions: []
      }
    }

    it('should remove the specified story', function() {
      const initialState = createStory(createStory(undefined, story1), story2);
      expect(initialState.hasIn(['stories', 'storyname'])).to.equal(true);
      const nextState = deleteStory(initialState, 'storyname');
      expect(nextState.hasIn(['stories', 'storyname'])).to.equal(false);
      expect(nextState.hasIn(['stories', 'storyname2'])).to.equal(true);
    });

    it('should return unaltered state if specified story does not exist', function() {
      const initialState = createStory(createStory(undefined, story1), story2);
      expect(initialState.hasIn(['stories', 'storyname'])).to.equal(true);
      const nextState = deleteStory(initialState, 'WRONGstoryname');
      expect(nextState).to.equal(initialState);
    });
  });
});
