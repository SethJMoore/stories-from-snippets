import makeStore from './src/store';
import startServer from './src/server';

const DAY = 86400000;
export const store = makeStore();
export const buildStorySettings = ({password = 'PASS',
                                    startTime = Date.now(),
                                    submittingInterval = 60000,
                                    votingInterval = 10000,
                                    story = [],
                                    submissions = []} = {}) => {
  return {
    currentRound: {
      startTime: startTime,
        endTime: startTime + submittingInterval,
    },
      password: password,
      submittingInterval: submittingInterval,
      votingInterval: votingInterval,
      story: story,
      submissions: submissions,
      expiration: startTime + DAY
  };
};

startServer(store, buildStorySettings);

